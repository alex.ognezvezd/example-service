#!/bin/bash
docker network create dalaran
cp .env.dev .env
docker-compose up -d db cache

go run ./cmd/rpc/main.go 

docker-compose down