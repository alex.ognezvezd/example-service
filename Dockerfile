# Build stage
FROM golang:1.19.7-alpine AS build
WORKDIR /go/src/gitlab.com/golight/example-service

COPY go.mod go.sum ./
RUN go mod download && apk add --no-cache ca-certificates

COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-s -w -extldflags "-static"' -o /app ./cmd/rpc/

# Final stage
FROM scratch
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /go/src/gitlab.com/golight/example-service/static /static
COPY --from=build /app /app


CMD ["/app"]
